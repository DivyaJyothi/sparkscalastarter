name := "test"
version := "3.3.2"
scalaVersion := "2.11.8"

libraryDependencies += "org.scala-lang" % "scala-compiler" % "2.11.8"
libraryDependencies += "org.scala-lang" % "scala-reflect" % "2.11.8"

libraryDependencies += "org.scala-lang.modules" % "scala-parser-combinators_2.11" % "1.0.4"
libraryDependencies += "org.scala-lang.modules" % "scala-xml_2.11" % "1.0.4"

libraryDependencies += "org.apache.spark" % "spark-core_2.11" % "2.1.0"
libraryDependencies += "org.apache.spark" % "spark-sql_2.11" % "2.1.0"

libraryDependencies += "org.codehaus.jettison" % "jettison" % "1.3.7"
libraryDependencies += "com.google.code.gson" % "gson" % "2.5"

libraryDependencies += "org.apache.poi" % "poi-ooxml" % "3.15"
libraryDependencies += "com.google.re2j" % "re2j" % "1.1"
libraryDependencies += "com.joestelmach" % "natty" % "0.11"

libraryDependencies += "com.squareup.okhttp3" % "okhttp" % "3.9.0"
libraryDependencies += "com.amazonaws" % "aws-java-sdk-s3" % "1.11.232"
