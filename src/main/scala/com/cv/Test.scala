package com.cv
import org.apache.log4j.Logger
import org.apache.spark
import org.apache.spark.sql.SparkSession
import org.apache.spark.{SparkConf, sql}

object Test { //extends App

  def main(args: Array[String]): Unit = {
    val sparkConf = new SparkConf().setAppName("EagleEyeValidateSmsRegex")
    sparkConf.set("spark.sql.caseSensitive", "true")
    sparkConf.setMaster("local[*]")
    sparkConf.set("spark.sql.warehouse.dir", "file:///")
    val sparkSession = SparkSession.builder().config(sparkConf).getOrCreate()

    val junkSmsLookup =  sparkSession.read.textFile("F:\\spark\\test\\src\\main\\scala\\com\\cv\\banking_junk_sms.csv")
    val cnt = junkSmsLookup.count()
    println("\n\n\n\n\n")
    println("HELLO WORLD "+cnt)

    val log = Logger.getLogger("NILESH")
    log.error("Divya Angry")
    log.info("Deeskha focused")
  }
}
